package pt.tiagoedgar.service.acl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;
import pt.tiagoedgar.util.ArticleUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class StaticAccessControlTest extends AccessControlTest{

    @BeforeEach
    void setUp() {
        accessControl = new StaticAccessControl();
    }

    @DisplayName("Mocking Static methods")
    @Nested
    class MockStaticUtils {

        @DisplayName("Absurd: No subscriber can access Locked article")
        @Test
        void mockExample1Test() {
            //Arrange
            Article article = new Article("Locked article", true);
            User user = new User("No Subscriber", false);
            boolean expectedAccess = true; //Just to test that the mock is working

            //Act && Assert
            try (MockedStatic<ArticleUtils> articleUtilsMockedStatic = Mockito.mockStatic(ArticleUtils.class)) {
                articleUtilsMockedStatic.when(() -> ArticleUtils.canAccess(article, user)).thenReturn(expectedAccess);

                //Act
                boolean canAccess = accessControl.canAccess(user, article);

                //Assert
                assertTrue(canAccess);
            }
        }

        @DisplayName("Absurd: Subscriber can not access Public article")
        @Test
        void mockExample2Test() {
            //Arrange
            Article article = new Article("Public article", false);
            User user = new User("Subscriber", true);
            boolean expectedAccess = false; //Just to test that the mock is working

            //Act && Assert
            try (MockedStatic<ArticleUtils> articleUtilsMockedStatic = Mockito.mockStatic(ArticleUtils.class)) {
                articleUtilsMockedStatic.when(() -> ArticleUtils.canAccess(article, user)).thenReturn(expectedAccess);

                //Act
                boolean canAccess = accessControl.canAccess(user, article);

                //Assert
                assertFalse(canAccess);
            }
        }
    }
}