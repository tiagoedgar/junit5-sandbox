package pt.tiagoedgar.service.acl;

import org.junit.jupiter.api.*;
import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled()
@DisplayName("This is just the parent class")
public class AccessControlTest {

    AccessControl accessControl;

    @DisplayName("User is Subscriber")
    @Nested
    class SubscriberTests {

        User user;
        @BeforeEach
        void setUp() {
            user = new User("Subscriber", true);
        }

        @DisplayName("Can access Public Articles")
        @Test
        void publicArticleTest() {
            //Arrange
            Article article = new Article("Public article", false);

            //Act
            boolean canAccess = accessControl.canAccess(user, article);

            //Assert
            assertTrue(canAccess);
        }

        @DisplayName("Can access Locked Articles")
        @Test
        void LockedArticleTest() {
            //Arrange
            Article article = new Article("Locked article", true);

            //Act
            boolean canAccess = accessControl.canAccess(user, article);

            //Assert
            assertTrue(canAccess);
        }
    }


    @DisplayName("User not Subscriber")
    @Nested
    class NotSubscriberTests {

        User user;
        @BeforeEach
        void setUp() {
            user = new User("Not Subscriber", false);
        }

        @DisplayName("Can access Public Articles")
        @Test
        void publicArticleTest() {
            //Arrange
            Article article = new Article("Public article", false);

            //Act
            boolean canAccess = accessControl.canAccess(user, article);

            //Assert
            assertTrue(canAccess);
        }

        @DisplayName("Can not access Locked Articles")
        @Test
        void LockedArticleTest() {
            //Arrange
            Article article = new Article("Locked article", true);

            //Act
            boolean canAccess = accessControl.canAccess(user, article);

            //Assert
            assertFalse(canAccess);
        }
    }
}
