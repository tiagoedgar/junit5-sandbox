package pt.tiagoedgar;

import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;
import pt.tiagoedgar.service.acl.AccessControl;
import pt.tiagoedgar.service.acl.ArticleAccessControl;
import pt.tiagoedgar.service.acl.StaticAccessControl;

public class Main {
    private static User notSubscriber;
    private static User subscriber;

    public static void main(String[] args) {
        System.out.println("JUnit 5 Demo");
        notSubscriber = new User("Tiago", true);
        subscriber = new User("Edgar", false);


        System.out.println(" ----------- Deprecated -----------");
        CheckAccessExample(
                new StaticAccessControl(),
                new Article("Public article"),
                new Article("Locked article")
                );


        System.out.println(" ----------- Refactor -----------");
        CheckAccessExample(
                new ArticleAccessControl(),
                new Article("Public article", false),
                new Article("Locked article", true)
        );

    }

    private static void CheckAccessExample(AccessControl accessControl, Article publicArticle, Article lockedArticle) {

        System.out.println("Using ACL --> " + accessControl.getClass().getSimpleName());

        boolean canAccess = accessControl.canAccess(notSubscriber, publicArticle);
        System.out.println(notSubscriber + " can read " + publicArticle + " | --> " + canAccess);

        canAccess = accessControl.canAccess(notSubscriber, lockedArticle);
        System.out.println(subscriber + " can read " + lockedArticle + " | --> " + canAccess);

        canAccess = accessControl.canAccess(subscriber, publicArticle);
        System.out.println(subscriber + " can read " + publicArticle + " | --> " + canAccess);

        canAccess = accessControl.canAccess(subscriber, lockedArticle);
        System.out.println(subscriber + " can read " + lockedArticle + " | --> " + canAccess);
    }
}