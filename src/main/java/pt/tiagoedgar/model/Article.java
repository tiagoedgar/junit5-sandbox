package pt.tiagoedgar.model;

public class Article {
    private String name;
    private boolean locked;

    public Article(String name, boolean locked) {
        this.name = name;
        this.locked = locked;
    }

    public Article(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    @Override
    public String toString() {
        return "Article{" +
                "name='" + name + '\'' +
                ", locked=" + locked +
                '}';
    }
}
