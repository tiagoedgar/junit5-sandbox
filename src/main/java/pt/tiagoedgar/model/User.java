package pt.tiagoedgar.model;

public class User {
    private String name;
    private boolean subscriber;

    public User(String name, boolean subscriber) {
        this.name = name;
        this.subscriber = subscriber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSubscriber() {
        return subscriber;
    }

    public void setSubscriber(boolean subscriber) {
        this.subscriber = subscriber;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", subscriber=" + subscriber +
                '}';
    }
}
