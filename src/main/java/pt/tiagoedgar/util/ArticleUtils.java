package pt.tiagoedgar.util;

import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Imagine something that we are trying to refactor.
 * I just want to test static methods first
 */
@Deprecated
public class ArticleUtils {

    static List<Article> lockedArticles = Arrays.asList(new Article("Locked article"));

    public static boolean canAccess(Article article, User user){
        if(user.isSubscriber()){
            return true;
        }

        boolean locked = lockedArticles.stream()
                .filter(lockedArticle -> lockedArticle.getName().equals(article.getName()))
                .findFirst()
                .isPresent();

        System.out.println("Article '"+article.getName()+"' is locked? " + locked);
        return !locked;
    }
}
