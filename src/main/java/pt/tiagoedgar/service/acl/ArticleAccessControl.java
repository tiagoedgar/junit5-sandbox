package pt.tiagoedgar.service.acl;

import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;

public class ArticleAccessControl implements AccessControl{

    @Override
    public boolean canAccess(User user, Article article){
        System.out.println(user + " | " + article);
        if(!article.isLocked() || user.isSubscriber()){
            System.out.println("User can read the article");
            return true;
        } else {
            System.err.println("User can not read the article");
            return false;
        }
    }
}
