package pt.tiagoedgar.service.acl;

import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;
import pt.tiagoedgar.util.ArticleUtils;

/**
 * Imagine something that we are trying to refactor.
 * I just want to test static methods first
 */
@Deprecated
public class StaticAccessControl implements AccessControl {


    @Override
    public boolean canAccess(User user, Article article) {
        boolean canAccess = ArticleUtils.canAccess(article, user);
        return canAccess;
    }
}
