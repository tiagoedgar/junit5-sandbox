package pt.tiagoedgar.service.acl;

import pt.tiagoedgar.model.Article;
import pt.tiagoedgar.model.User;

public interface AccessControl {
    boolean canAccess(User user, Article article);
}
