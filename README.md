# JUnit5 Sandbox

This is just a project to test the Junit 5.

The main idea is to simulate a refactor process where we want to assure that the user permissions are preserved.

> **Context:** A website with *public* and *paid* **articles** that have **users** that can have (or not) a *subscription* to view *paid/locked* articles.

## Testing new implementation

In order to assure those permissions, I will perform some **common jUnit tests for both AccessControl implementations**.

The idea is to test the concrete class implementations (*StaticsAccessControlTest* and *ArticleAccessControlTest*) using the tests described in the common interface/parent test class: *AccessControlTest*.

Just run the tests and you will understand.


## Mocking statics

Since [Mockito version 3.4.0 that we can mock static methods.](https://asolntsev.github.io/en/2020/07/11/mockito-static-methods/)

This project also has in **StaticsAccessControlTest** some **absurd examples** demonstrating that it's possible to mock static methods using only Mockito (without PowerMockito).